# EZITXT Rest Api Client

## Getting Started

Install via npm:
```
  npm install --save ezitxt-client
```

Require the package and get started:
```
  var EziClient = require('ezitxt-client');
  var client = new EziClient({ api_key: "API_KEY_GOES_HERE" });

  client.sendMessage({
    recipient: 64123456789
    content: "Holy moly that was ezi" 
  }).then(
    function (json) {
      console.log(json);
    },
    function (err) {
      console.log(err);
    }
  );
```

## Methods

### sendMessage
Sends a message to a existing contact or specified recipient.

```
client.sendMessage({
    recipient: 64123456789
    content: "Holy moly that was ezi" 
  });
```

## Roadmap

- Support more API endpoints
- Add tests (when/if ezitxt has text api support)