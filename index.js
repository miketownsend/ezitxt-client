/**
 * #EZITXT Rest Client
 *
 * Rest API Client for the EZITXT sms service
 * 
 * @class EziClient
 */
var EziClient = function (options) {
  if( !options || !options.api_key ) {
    throw new Error('Invalid options, minimum -> { api_key: "ABCD..." }');
  }

  this.api_root = options.api_root || "https://eziapi.com";
  this.api_key = options.api_key;
};

EziClient.prototype.sendMessage = require("./lib/sendMessage");

module.exports = EziClient;