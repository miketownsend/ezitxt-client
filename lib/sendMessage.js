var request = require('request');

/**
 * Sends a SMS message to a contact or recipient
 *
 * @for  EziClient
 * @param  {object} options
 * 
 * ```
 * {
 *  recipient: 6412345678, // MSISDN number to send to, required if 'contact_id' not specified.
 *  contact_id: 12345678, // ID of contact to send to.
 *  template_id: 12345678, // ID of template to use/
 *  content: "message to send", // Message to send, required if 'template_id' not specified.
 *  reply: false, // Set to true if sending a replay a specific message in the system.
 *  replay_to: 12345678, // ID of message being replied to.
 *  uid: "ABCDEFG" // Unique identifier for the message, used to prevent accidental duplication of sends.
 * }
 * ```
 * 
 * @return {object}
 *
 * ```
 * {
 *    id: <integer>
 *    direction:  <string>
 *    msisdn: <integer>
 *    units:  <integer>
 *    encoding: <string>
 *    timestamp:  <timestamp>
 *    last_updated: <timestamp>
 *    status: <string>
 *    message_id: <integer>
 *    keyword_id: <integer>
 *    contact_id: <integer>
 *    parent_id:  <integer>
 *  }
 * ```
 */

module.exports = function (options) {
  // check for valid options
  if( !options ) {
    throw new Error('No options specified.');
  }
  if( !options.recipient && !options.contact_id) {
    throw new Error("Missing 'recipient' or 'contact_id' option.");
  }
  if( !options.content && !options.template_id ) {
    throw new Error("Missing 'content' field."); 
  }

  var base = {
    recipient: options.recipient,
    content: options.content,
    uid: Date.now() + ""
  };

  return new Promise(function(resolve, reject) {
    request({
      url: this.api_root + "/v3/sms",
      method: "POST",
      json: true,
      headers: {
          "content-type": "application/json",
          "key": this.api_key
      },
      body: base
    }, function (err, response, body) {
      if( err || response.statusCode !== 200 ) {
        if( err ) { 
          console.log('err', err);
          reject(err);
        } else {
          reject(response.toJSON());
        }
      } else {
        resolve(body);
      }
    });
  }.bind(this));
};